import View from './view';
import Fighter from './fighter';

class Arena extends View {
    constructor() {
        super();
        this.fightLog  = '';
        this.btn_fight = document.getElementById('btn_fight');
        this.btn_clear = document.getElementById('btn_clear');
        this.loadFightListeners();
        this.loadClearListeners();
    }

    static loadingElement = document.getElementById('loading-overlay');
    static resultElement = document.getElementById('aresult');
    static rootElement = document.getElementById('afighters');
    static transcriptElement = document.getElementById('transcript');

    arenaFightersMap = new Map();

    createFighter(fighterId) {
        if (this.arenaFightersMap.has(fighterId)) {
            let fighterDetails = this.arenaFightersMap.get(fighterId);
            const detailsElement = this.createFighterDetails(fighterDetails);

            this.element = this.createElement({tagName: 'div', className: 'arena-fighter'});
            this.element.append(detailsElement);

            Arena.rootElement.append(this.element);
        } else {
            alert('Something wrong! The is no fighter with such ID.');
        }

    }

    createFighterDetails(fighterDetails) {
        const detailsElement = this.createElement({tagName: 'span', className: 'arena-fighter-details'});
        detailsElement.innerText = fighterDetails.name
            + ': [ h:' + fighterDetails.health + ' | a:' + fighterDetails.attack + ' | d:' + fighterDetails.defense + ' ]';

        return detailsElement;
    }


    fight() {
        let fighters = [...this.arenaFightersMap];

        // random start fighter
        fighters.sort(() => Math.random() - 0.5);

        let fighter1 = fighters[0][1];
        let fighter2 = fighters[1][1];

        fighter1 = new Fighter(fighter1);
        fighter2 = new Fighter(fighter2);

        let lap = 1;
        let result = '';
        let fighting = true;

        let F_1_Name   = fighter1.fighter.name;
        let F_1_Health = fighter1.fighter.health;
        let F_2_Name   = fighter2.fighter.name;
        let F_2_Health = fighter2.fighter.health;

        while (fighting) {
            let F_1_Defence = fighter1.getBlockPower();
            let F_1_Attack  = fighter1.getHitPower();

            let F_2_Defence = fighter2.getBlockPower();
            let F_2_Attack  = fighter2.getHitPower();

            this.logAdd('---------------- Round [' + lap +'] ----------------');

            // 1
            if (fighting) {
                this.logAdd(F_2_Name + ': ' + (F_2_Health + '[health] + ' + F_2_Defence + '[block]') + ' - ' + F_1_Attack + '[damage]');
                F_2_Health = F_2_Health + F_2_Defence - F_1_Attack;
                this.logAdd(F_2_Name + ': ' + F_2_Health);

                if (F_2_Health <= 0) {
                    fighting = false;
                    result = F_1_Name + ' WIN!!!';
                }
            }

            // 2
            if (fighting) {
                this.logAdd('---');

                this.logAdd(F_1_Name + ': ' + (F_1_Health + '[health] + ' + F_1_Defence + '[block]') + ' - ' + F_2_Attack + '[damage]');
                F_1_Health = F_1_Health + F_1_Defence - F_2_Attack;
                this.logAdd(F_1_Name + ': ' + F_1_Health);

                if (F_1_Health <= 0) {
                    fighting = false;
                    result = F_2_Name + ' WIN!!!';
                }
            }

            this.logAdd('---------------- Round [' + lap +'] ----------------');
            this.logAdd('\n');
            lap++;
        }

        this.logAdd(result);

        Arena.resultElement.innerHTML = '<h1>' + result + '</h1>';
        Arena.transcriptElement.innerHTML = this.fightLog.replace(/\n/gi, '<br/>');
        console.log(this.fightLog);
    }

    // Create event listeners for "fight" button
    loadFightListeners() {
        this.btn_fight.addEventListener('click', () => {
            if (this.arenaFightersMap.size === 2) {
                Arena.loadingElement.style.visibility = 'visible';
                Arena.resultElement.innerHTML = '';
                Arena.transcriptElement.innerHTML = '';

                this.logClear();
                let that = this;
                setTimeout(function () {
                    that.fight();
                    Arena.loadingElement.style.visibility = 'hidden';
                }, 1000);
            } else {
                alert('Choose a fighter!');
            }
        });
    }

    // Create event listeners for "clear" button
    loadClearListeners() {
        this.btn_clear.addEventListener('click', () => {
            Arena.rootElement.innerHTML   = '';
            Arena.resultElement.innerHTML = '';
            Arena.transcriptElement.innerHTML = '';
            this.arenaFightersMap.clear();
            this.logClear();
        });
    }

    logAdd(message) {
        this.fightLog += message + '\n';
    }

    logClear() {
        this.fightLog = '';
    }
}

export default Arena;