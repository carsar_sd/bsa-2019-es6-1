import View from './view';
import FighterView from './fighterView';
import FighterDetails from './services/fighterDetails';
import {fighterService} from './services/fightersService';
import ArenaView from "./arenaView";

class FightersView extends View {
    constructor(fighters) {
        super();
        this.handleClick = this.handleFighterClick.bind(this);
        this.createFighters(fighters);
    }

    static loadingElement = document.getElementById('loading-overlay');

    fightersDetailsMap = new Map();
    arenaView = new ArenaView();

    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleClick);
            return fighterView.element;
        });

        this.element = this.createElement({tagName: 'div', className: 'fighters'});
        this.element.append(...fighterElements);
    }

    async handleFighterClick(event, fighter) {
        /** -- action loader show **/
        FightersView.loadingElement.style.visibility = 'visible';

        /** -- get & set fighter details **/
        let fighterDetails = await fighterService.getFighterDetails(fighter._id);

        if (!this.fightersDetailsMap.has(fighter._id)) {
            this.fightersDetailsMap.set(fighter._id, fighterDetails);
        }

        /** -- prepare fighter details for popup **/
        const fighterBlock = document.querySelectorAll("*[data-id='" + fighter._id + "']")[0];
        const detailsPopup = new FighterDetails('fighter-details', this.fightersDetailsMap, this.arenaView);
        let fighterInfo    = this.fightersDetailsMap.get(fighter._id);

        if (typeof(fighterInfo) !== 'undefined') {
            Object.entries(fighterInfo).forEach(([key, value]) => {
                let field = document.getElementById(`f${key}`);

                if (document.body.contains(field)) {
                    field.value = value;
                }
            });
        }

        /** -- action loader hide **/
        FightersView.loadingElement.style.visibility = 'hidden';
        /** -- popup show **/
        detailsPopup.show(fighterBlock.offsetLeft, fighterBlock.offsetTop);
    }
}

export default FightersView;