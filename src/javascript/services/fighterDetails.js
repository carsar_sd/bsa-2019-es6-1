class FighterDetails {
    constructor(id, detailsMap, arenaView) {
        this.elem       = document.getElementById(id);
        this.btn_save   = document.getElementById('btn_save');
        this.btn_start  = document.getElementById('btn_start');
        this.detailsMap = detailsMap;
        this.arenaView  = arenaView;
        this.loadHideListeners();
        this.loadSaveListeners();
        this.loadStartListeners();
    };

    show(mLeft, mTop, callback) {
        this.elem.firstElementChild.style.marginLeft = mLeft + 'px';
        this.elem.firstElementChild.style.marginTop = mTop + 'px';
        this.elem.style.display = "block";
        if (callback) callback();
    };

    hide(callback) {
        this.elem.style.display = "none";
        this.createCloseEvent();
        if (callback) callback();
    };

    createCloseEvent() {
        let event = new Event('ModalClose');
        document.dispatchEvent(event);
    }

    // Create event listeners for "close modal"
    loadHideListeners() {
        // Hide modal when you press close
        this.elem.addEventListener('click', e => {
            if (e.target.classList.contains("close-modal")) {
                this.hide();
            }
        });

        // Also hide modal when you press outside
        this.elem.addEventListener('click', e => {
            if (e.target.classList.contains('modal-fighter')) {
                this.hide();
            }
        });
    }

    // Create event listeners for "save" button
    loadSaveListeners() {
        this.btn_save.addEventListener('click', () => {
            let detailsMap = this.detailsMap;
            let fighterId  = document.querySelectorAll("#fdetails input[type=hidden]")[0].value;
            let elements   = document.querySelectorAll("#fdetails input[type=text][name]");
            let hideOrNot  = true;

            elements.forEach(function (element) {
                if (element.value == parseInt(element.value)) {
                    detailsMap.get(fighterId)[element.name] = element.value;
                    element.style.borderColor = '';
                } else {
                    element.style.borderColor = '#d93025';
                    hideOrNot = false;
                }
            });

            if (hideOrNot) {
                this.hide();
            }
        }, { once: true });
    }

    // Create event listeners for "start" button
    loadStartListeners() {
        this.btn_start.addEventListener('click', () => {
            let fighterId = document.querySelectorAll("#fdetails input[type=hidden]")[0].value;
            let arenaView = this.arenaView;
            let arenaMap  = arenaView.arenaFightersMap;

            if (!arenaMap.has(fighterId)) {
                let fighterDetails = this.detailsMap.get(fighterId);
                let hideOrNot = true;

                if (arenaMap.size < 2) {
                    arenaMap.set(fighterId, fighterDetails);
                    arenaView.createFighter(fighterId);
                } else {
                    hideOrNot = false;
                    alert('Arena has already filled!');
                }

                if (hideOrNot) {
                    this.hide();
                }
            }/* else { // странное поведение MAP-ов
                alert('This fighter has already added!');
            }*/
        }, { once: true });
    }
}

export default FighterDetails;
