class Fighter {
    constructor(fighter) {
        this.fighter = fighter;
    }

    getHitPower() {
        let criticalHitChance = Math.floor(Math.random() * 2) + 1;
        return this.fighter.attack * criticalHitChance;
    }

    getBlockPower() {
        let dodgeChance = Math.floor(Math.random() * 2) + 1;
        return this.fighter.defense * dodgeChance;
    }
}

export default Fighter;